package userInterface;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VerifExpReg {
    public static boolean isValidEmail(String email) {
        String regExp="^[\\w_.-]+@[\\w.-]+\\.[a-z]{2,}$";
        return email.matches(regExp);
    }

    public static boolean isValidNomPrenom(String nom) {
        String regExp="^[a-z -]{2,15}$";
        return nom.matches(regExp);
    }

    public static boolean isValidDate(String date,String pattern) {
        try {
            Date simple=new SimpleDateFormat(pattern).parse(date);
            Format format=new SimpleDateFormat(pattern);
            if (!date.equals(format.format(simple)))
                return false;
            return true;
        }
        catch (@SuppressWarnings("unused") ParseException e) {
            return false;
        }
    }

    public static boolean isValidWord(String word) {
        String regExp="^[\\w -]{1,30}$";
        return word.matches(regExp);
    }

    public static boolean isValidPostalCode(String postalCode) {
        String regExp="^[0-9]{5}$";
        return postalCode.matches(regExp);
    }
}