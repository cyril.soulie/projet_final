package userInterface;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import dataAccessObject.Comment;
import dataAccessObject.DAOComment;
import dataAccessObject.DAOProduct;
import dataAccessObject.DAORequest;
import dataAccessObject.DAOSelection;
import dataAccessObject.DAOUser;
import dataAccessObject.Product;
import dataAccessObject.ProductPopularityComparator;
import dataAccessObject.Request;
import dataAccessObject.Selection;
import dataAccessObject.User;

public class UserInterface {
    private static Scanner scanner;
    private static User user;

    public static void main(String[] args) {
        Logger.getLogger("org.hibernate").setLevel(Level.OFF);
        scanner=new Scanner(System.in);
        user=new User();
        menu();
        scanner.close();
    }

    private static void menu() {
        if (user.getAuthentificated()) {
            System.out.println("Tapez 0 pour vous déconnecter.\nTapez 1 pour passer une commande.");
        }
        else {
            System.out.println("Tapez 0 pour quitter.\nTapez 1 pour vous connecter.");
        }
        System.out.println("Tapez 2 pour afficher la liste des produits disponibles.\nTapez 3 pour afficher les produits les plus populaires.\nTapez 4 pour afficher les actualités du magasin.\nTapez 5 pour afficher les commentaires sur un produit.\nTapez 6 pour contacter le webmaster.");
        if (user.getAuthentificated()) {
            System.out.println("Tapez 7 pour afficher l'historique de vos commandes.\nTapez 8 pour accéder à vos commentaires.\nTapez 9 pour mettre à jour votre profil.");
            if (user.getAdmin()) {
                System.out.println("Tapez 10 pour gérer les utilisateurs et leurs commentaires.\nTapez 11 pour gérer le catalogue des produits.");
            }
        }
        switch (scanner.nextLine()) {
            case "0":
                if (user.getAuthentificated()) {
                    user.setAuthentificated(false);
                    menu();
                }
                break;
            case "1":
                if (user.getAuthentificated()) {
                    selectRequestType(user,new Request());
                }
                else {
                    connection();
                }
                menu();
                break;
            case "2":
                selectRequestType(user,null);
                menu();
                break;
            case "3":
                displayPopular();
                menu();
                break;
            case "4":
                displayNews();
                menu();
                break;
            case "5":
                displayCommentsByProduct();
                menu();
                break;
            case "6":
                contact();
                menu();
                break;
            case "7":
                if (user.getAuthentificated()) {
                    displayRequest(user);
                }
                menu();
                break;
            case "8":
                if (user.getAuthentificated()) {
                    comments(user);
                }
                menu();
                break;
            case "9":
                if (user.getAuthentificated()) {
                    updateUser(user);
                }
                menu();
                break;
            case "10":
                if (user.getAuthentificated()&&user.getAdmin()) {
                    manageUsers();
                }
                menu();
                break;
            case "11":
                if (user.getAuthentificated()&&user.getAdmin()) {
                    manageSupply();
                }
                menu();
                break;
            default:
                menu();
        }
    }

    private static void connection() {
        System.out.println("Veuillez saisir votre adresse e-mail :");
        String string=scanner.nextLine();
        if (VerifExpReg.isValidEmail(string)) {
            user=DAOUser.selectUserById(string);
            if (user==null) {
                user=new User();
                user.seteMail(string);
                insertUser(user);
                user.setAuthentificated(true);
                DAOUser.updateUser(user);
            }
            else {
                System.out.println("Veuillez saisir votre mot de passe :");
                if (user.getPassword().equals(scanner.nextLine())) {
                    user.setAuthentificated(true);
                }
            }
        }
        else {
            System.out.println("Veuillez entrer e-mail valide.\n");
            connection();
        }
    }

    private static void insertUser(User u) {
        DAOUser.insertUser(u);
        updateLastName(u);
        updateFirstName(u);
        updateBirthdate(u);
        updateCompany(u);
        updatePassword(u);
        updateGender(u);
        updateNumber(u);
        updateStreet(u);
        updatePostalCode(u);
        updateCity(u);
    }

    private static void updateLastName(User u) {
        System.out.println("Veuillez saisir le nom :");
        String string=scanner.nextLine().toLowerCase();
        u.setLastName(string);
        if (!VerifExpReg.isValidNomPrenom(string)) {
            System.out.println("Veuillez entrer un nom valide.\n");
            updateLastName(u);
        }
    }

    private static void updateFirstName(User u) {
        System.out.println("Veuillez saisir le prénom :");
        String string=scanner.nextLine().toLowerCase();
        u.setFirstName(string);
        if (!VerifExpReg.isValidNomPrenom(string)) {
            System.out.println("Veuillez entrer un prénom valide.\n");
            updateFirstName(u);
        }
    }

    private static void updateBirthdate(User u) {
        System.out.println("Veuillez saisir la date de naissance (format : jj/mm/aaaa)");
        String string=scanner.nextLine();
        u.setBirthdate(string);
        if (!VerifExpReg.isValidDate(string,"dd/MM/yyyy")) {
            System.out.println("Veuillez entrer une date valide.\n");
            updateBirthdate(u);
        }
    }

    private static void updateCompany(User u) {
        System.out.println("Veuillez saisir la société :");
        String string=scanner.nextLine();
        u.setCompany(string);
        if (!VerifExpReg.isValidWord(string)) {
            System.out.println("Veuillez entrer un nom de société valide.\n");
            updateCompany(u);
        }
    }

    private static void updatePassword(User u) {
        System.out.println("Veuillez saisir un mot de passe :");
        String string=scanner.nextLine();
        if (!VerifExpReg.isValidWord(string)) {
            System.out.println("Veuillez entrer un mot de passe valide.\n");
            updatePassword(u);
        }
        System.out.println("Veuillez confirmer le mot de passe :");
        if (string.equals(scanner.nextLine())) {
            u.setPassword(string);
        }
        else {
            System.out.println("Les deux saisies sont différentes.\n");
            updatePassword(u);
        }
    }

    private static void updateGender(User u) {
        System.out.println("Veuillez saisir le genre.\nTapez f pour femme.\nTapez h pour homme.");
        switch (scanner.nextLine().trim().toLowerCase()) {
            case "f":
                u.setGender("Femme");
                break;
            case "h":
                u.setGender("Homme");
                break;
            default:
                updateGender(u);
        }
    }

    private static void updateNumber(User u) {
        System.out.println("Veuillez saisir le numéro de rue :");
        u.getAdress().setNumber(scanner.nextLine());
    }

    private static void updateStreet(User u) {
        System.out.println("Veuillez saisir le nom de la rue :");
        String string=scanner.nextLine();
        u.getAdress().setStreet(string);
        if (!VerifExpReg.isValidWord(string)) {
            System.out.println("Veuillez entrer un nom de rue valide.\n");
            updateStreet(u);
        }
    }

    private static void updatePostalCode(User u) {
        System.out.println("Veuillez saisir le code postal :");
        String string=scanner.nextLine();
        u.getAdress().setPostalCode(string);
        if (!VerifExpReg.isValidPostalCode(string)) {
            System.out.println("Veuillez entrer un code postal valide.\n");
            updatePostalCode(u);
        }
    }

    private static void updateCity(User u) {
        System.out.println("Veuillez saisir la ville :");
        String string=scanner.nextLine();
        u.getAdress().setCity(string);
        if (!VerifExpReg.isValidWord(string)) {
            System.out.println("Veuillez entrer un nom de ville valide.\n");
            updateCity(u);
        }
    }

    private static void selectRequestType(User u,Request r) {
        ArrayList<Product> aL=new ArrayList<>();
        System.out.println("Veuillez choisir un type de menu.\nTapez 1 pour un petit-déjeuner.\nTapez 2 pour un déjeuner.\nTapez 3 pour un goûter.\nTapez 4 pour un dîner.\nTapez 0 pour revenir au menu précédent.");
        switch (scanner.nextLine()) {
            case "0":
                break;
            case "1":
                aL=DAOProduct.selectProductByRequestType("breakfast");
                if (r==null||aL.size()==0) {
                    displayProductList(aL);
                    selectRequestType(u,r);
                }
                else {
                    r.setType("Breakfast");
                    preRequest(u,r,aL);
                }
                break;
            case "2":
                aL=DAOProduct.selectProductByRequestType("lunch");
                if (r==null||aL.size()==0) {
                    displayProductList(aL);
                    selectRequestType(u,r);
                }
                else {
                    r.setType("Lunch");
                    preRequest(u,r,aL);
                }
                break;
            case "3":
                aL=DAOProduct.selectProductByRequestType("snack");
                if (r==null||aL.size()==0) {
                    displayProductList(aL);
                    selectRequestType(u,r);
                }
                else {
                    r.setType("Snack");
                    preRequest(u,r,aL);
                }
                break;
            case "4":
                aL=DAOProduct.selectProductByRequestType("diner");
                if (r==null||aL.size()==0) {
                    displayProductList(aL);
                    selectRequestType(u,r);
                }
                else {
                    r.setType("Diner");
                    preRequest(u,r,aL);
                }
                break;
            default:
                selectRequestType(u,r);
        }
    }

    private static void displayProductList(ArrayList<Product> aL) {
        for (Product p:aL) {
            System.out.println("Identifiant : "+p.getId()+" , Type : "+p.getType()+" , Nom : "+p.getName()+" , Quantité unitaire (g ou ml) : "+p.getAmount()+" , Prix Unitaire : "+new DecimalFormat("#.00").format(p.getPrice())+" , Maximum : "+p.getSupply());
        }
        System.out.println("\n");
        if (aL.size()==0) {
            System.out.println("Aucun produit à afficher.\n");
        }
    }

    private static void preRequest(User u,Request r,ArrayList<Product> aL) {
        displayProductList(aL);
        System.out.println("Veuillez saisir l'identifiant du produit désiré parmi ceux de la liste ci-dessus.\nTapez v pour valider votre commande.\nTapez q pour un revenir au menu précédent.");
        String string=scanner.nextLine();
        switch (string) {
            case "q":
                selectRequestType(u,r);
                break;
            case "v":
                if (r.getSelectionList().size()>0) {
                    validation(u,r,aL);
                }
                else {
                    System.out.println("Votre commande ne contient aucune sélection.\n");
                    preRequest(u,r,aL);
                }
                break;
            default:
                try {
                    int id=Integer.parseInt(string);
                    boolean test=true;
                    for (int i=0;i<aL.size();i++) {
                        if (id==aL.get(i).getId()) {
                            test=false;
                            selectQuantity(u,r,aL,i);
                            i=aL.size();
                        }
                    }
                    if (test) {
                        preRequest(u,r,aL);
                    }
                }
                catch (@SuppressWarnings("unused") Throwable throwable) {
                    preRequest(u,r,aL);
                }
        }
    }

    private static void selectQuantity(User u,Request r,ArrayList<Product> aL,int index) {
        System.out.println("Veuillez saisir la quantité désirée (maximum : "+aL.get(index).getSupply()+") :");
        try {
            int quantity=Integer.parseInt(scanner.nextLine());
            if (quantity>0&&quantity<=aL.get(index).getSupply()) {
                boolean newSelection=true;
                for (Selection selection:r.getSelectionList()) {
                    if (selection.getProductId()==aL.get(index).getId()) {
                        selection.setQuantity(selection.getQuantity()+quantity);
                        newSelection=false;
                        break;
                    }
                }
                if (newSelection) {
                    r.getSelectionList().add(new Selection());
                    ((ArrayList<Selection>) r.getSelectionList()).get(((ArrayList<Selection>) r.getSelectionList()).size()-1).setProductId(aL.get(index).getId());
                    ((ArrayList<Selection>) r.getSelectionList()).get(((ArrayList<Selection>) r.getSelectionList()).size()-1).setType(aL.get(index).getType());
                    ((ArrayList<Selection>) r.getSelectionList()).get(((ArrayList<Selection>) r.getSelectionList()).size()-1).setName(aL.get(index).getName());
                    ((ArrayList<Selection>) r.getSelectionList()).get(((ArrayList<Selection>) r.getSelectionList()).size()-1).setAmount(aL.get(index).getAmount());
                    ((ArrayList<Selection>) r.getSelectionList()).get(((ArrayList<Selection>) r.getSelectionList()).size()-1).setPrice(aL.get(index).getPrice());
                    ((ArrayList<Selection>) r.getSelectionList()).get(((ArrayList<Selection>) r.getSelectionList()).size()-1).setQuantity(quantity);
                    ((ArrayList<Selection>) r.getSelectionList()).get(((ArrayList<Selection>) r.getSelectionList()).size()-1).setRequest(r);
                }
                r.setPrice(r.getPrice()+aL.get(index).getPrice()*quantity);
                aL.get(index).setSupply(aL.get(index).getSupply()-quantity);
                if (aL.get(index).getSupply()==0) {
                    aL.remove(index);
                }
                preRequest(u,r,aL);
            }
            else if (quantity==0) {
                preRequest(u,r,aL);
            }
            else {
                selectQuantity(u,r,aL,index);
            }
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            selectQuantity(u,r,aL,index);
        }
    }

    private static void validation(User u,Request r,ArrayList<Product> aL) {
        System.out.println("Veuillez saisir votre mode de réception.\nTapez r pour retrait.\nTapez l pour livraison (+10%).\nTapez q pour revenir au menu précédent.");
        String string=scanner.nextLine();
        if (string.equals("r")||string.equals("l")) {
            if (string.equals("l")) {
                r.setDelivery(true);
                r.setPrice(r.getPrice()*1.1);
            }
            r.setDate(LocalDate.now().toString());
            r.setUser(u);
            for (Selection selection:r.getSelectionList()) {
                Product product=DAOProduct.selectProductById(selection.getProductId());
                product.setPopularity(product.getPopularity()+selection.getQuantity());
                product.setSupply(product.getSupply()-selection.getQuantity());
                DAOProduct.updateProduct(product);
            }
            DAORequest.insertRequest(u,r);
            System.out.println("Votre commande a bien été enregistrée.\n");
        }
        else if (string.equals("q")) {
            preRequest(u,r,aL);
        }
        else {
            validation(u,r,aL);
        }
    }

    private static void displayPopular() {
        ArrayList<Product> arrayList=DAOProduct.selectAllProducts();
        Collections.sort(arrayList,new ProductPopularityComparator());
        for (int i=0;i<3;i++) {
            System.out.println("Identifiant : "+arrayList.get(i).getId()+" , Type : "+arrayList.get(i).getType()+" , Nom : "+arrayList.get(i).getName()+" , Quantité unitaire (g ou ml) : "+arrayList.get(i).getAmount()+" , Prix Unitaire : "+new DecimalFormat("#.00").format(arrayList.get(i).getPrice()));
        }
        System.out.println("\n");
    }

    private static void displayNews() {
        try (BufferedReader bufferedReader=new BufferedReader(new FileReader("news.txt"))) {
            System.out.println(bufferedReader.lines().collect(Collectors.joining("\n"))+"\n");
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            System.out.println("Aucune actualité à afficher.\n");
        }
    }

    private static void displayCommentsByProduct() {
        ArrayList<Product> arrayList=DAOProduct.selectAllProducts();
        int count=0;
        for (Product p:arrayList) {
            if (p.getCommentList().size()>0) {
                count++;
                System.out.println("Identifiant : "+p.getId()+" , Type : "+p.getType()+" , Nom : "+p.getName()+" , Quantité unitaire (g ou ml) : "+p.getAmount()+" , Prix Unitaire : "+new DecimalFormat("#.00").format(p.getPrice())+" , Maximum : "+p.getSupply());
            }
        }
        System.out.println("\n");
        if (count==0) {
            System.out.println("Aucun produit n'a encore été commenté.\n");
        }
        else {
            System.out.println("Veuillez saisir l'identifiant du produit pour lequel afficher les commentaires.\nTapez q pour un revenir au menu précédent.");
            String string=scanner.nextLine();
            switch (string) {
                case "q":
                    break;
                default:
                    try {
                        int id=Integer.parseInt(string);
                        boolean test=true;
                        for (int i=0;i<arrayList.size();i++) {
                            if (id==arrayList.get(i).getId()) {
                                test=false;
                                ArrayList<Comment> aL=DAOComment.selectCommentByProduct(DAOProduct.selectProductById(id));
                                for (Comment c:aL) {
                                    System.out.println(c.getContent());
                                }
                                System.out.println("\n");
                                if (aL.size()==0) {
                                    System.out.println("Aucun commentaire disponible sur ce produit.\n");
                                }
                                i=arrayList.size();
                                displayCommentsByProduct();
                            }
                        }
                        if (test) {
                            System.out.println("Ce produit n'est pas référencé.\n");
                        }
                    }
                    catch (@SuppressWarnings("unused") Throwable throwable) {
                        displayCommentsByProduct();
                    }
            }
        }
    }

    private static void contact() {
        ArrayList<User> arrayList=DAOUser.selectUserByAdmin();
        System.out.println("Veuillez envoyer votre message à l'une des adresses suivantes :");
        for (User u:arrayList) {
            System.out.println(u.geteMail());
        }
        System.out.println("\n");
    }

    private static void displayRequest(User u) {
        ArrayList<Request> arrayList=DAORequest.selectRequestByUser(u);
        for (Request r:arrayList) {
            System.out.println(r.toString());
        }
        System.out.println("\n");
        if (arrayList.size()==0) {
            System.out.println("Vous n'avez aucune commande enregistrée.\n");
        }
    }

    private static void comments(User u) {
        ArrayList<Comment> arrayList=new ArrayList<>();
        System.out.println("Veuillez choisir l'action à effectuer.\nTapez 1 pour supprimer un commentaire.\nTapez 2 pour mettre à jour un commentaire.");
        if (user.geteMail().equals(u.geteMail())) {
            System.out.println("Tapez 3 pour insérer un commentaire sur un produit que vous avez commandé mais pas encore commenté.");
        }
        System.out.println("Tapez 0 pour revenir au menu précédent.");
        switch (scanner.nextLine()) {
            case "0":
                break;
            case "1":
                arrayList=DAOComment.selectCommentByUser(u);
                displayCommentList(arrayList);
                if (arrayList.size()>0) {
                    deleteComment(u,arrayList);
                }
                if (user.geteMail().equals(u.geteMail())) {
                    comments(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "2":
                arrayList=DAOComment.selectCommentByUser(u);
                displayCommentList(arrayList);
                if (arrayList.size()>0) {
                    updateComment(u,arrayList);
                }
                if (user.geteMail().equals(u.geteMail())) {
                    comments(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "3":
                if (user.geteMail().equals(u.geteMail())) {
                    insertComment(u);
                    comments(u);
                }
                else {
                    manageUsers();
                }
                break;
            default:
                if (user.geteMail().equals(u.geteMail())) {
                    comments(u);
                }
                else {
                    manageUsers();
                }
        }
    }

    private static void displayCommentList(ArrayList<Comment> arrayList) {
        for (Comment c:arrayList) {
            System.out.println(c.toString());
        }
        System.out.println("\n");
        if (arrayList.size()==0) {
            System.out.println("Il n'y a aucun commentaire à afficher.\n");
        }
    }

    private static void deleteComment(User u,ArrayList<Comment> arrayList) {
        System.out.println("Veuillez saisir l'identifiant du commentaire à supprimer.\nTapez q pour un revenir au menu précédent.");
        String string=scanner.nextLine();
        switch (string) {
            case "q":
                comments(u);
                break;
            default:
                try {
                    int id=Integer.parseInt(string);
                    boolean test=true;
                    for (int i=0;i<arrayList.size();i++) {
                        if (id==arrayList.get(i).getId()) {
                            test=false;
                            DAOComment.deleteComment(arrayList.get(i).getId());
                            i=arrayList.size();
                        }
                    }
                    if (test) {
                        deleteComment(u,arrayList);
                    }
                }
                catch (@SuppressWarnings("unused") Throwable throwable) {
                    deleteComment(u,arrayList);
                }
        }
    }

    private static void updateComment(User u,ArrayList<Comment> arrayList) {
        System.out.println("Veuillez saisir l'identifiant du commentaire à mettre à jour.\nTapez q pour un revenir au menu précédent.");
        String string=scanner.nextLine();
        switch (string) {
            case "q":
                comments(u);
                break;
            default:
                try {
                    int id=Integer.parseInt(string);
                    boolean test=true;
                    for (int i=0;i<arrayList.size();i++) {
                        if (id==arrayList.get(i).getId()) {
                            test=false;
                            System.out.println("Veuillez saisir le commentaire sur le produit :");
                            arrayList.get(i).setContent(scanner.nextLine());
                            arrayList.get(i).setDate(LocalDate.now().toString());
                            DAOComment.updateComment(arrayList.get(i));
                            i=arrayList.size();
                        }
                    }
                    if (test) {
                        updateComment(u,arrayList);
                    }
                }
                catch (@SuppressWarnings("unused") Throwable throwable) {
                    updateComment(u,arrayList);
                }
        }
    }

    private static void insertComment(User u) {
        ArrayList<Selection> arrayList=DAOSelection.selectUncommentedSelectionByUser(u);
        for (Selection s:arrayList) {
            System.out.println(s.toString());
        }
        if (arrayList.size()==0) {
            System.out.println("Vous avez déjà commenté tous les produits que vous avez commandé.\n");
        }
        else {
            System.out.println("Veuillez saisir l'identifiant du produit à commenter parmi ceux de la liste ci-dessus.\nTapez q pour un revenir au menu précédent.");
            String string=scanner.nextLine();
            switch (string) {
                case "q":
                    comments(u);
                    break;
                default:
                    try {
                        int id=Integer.parseInt(string);
                        boolean test=true;
                        for (int i=0;i<arrayList.size();i++) {
                            if (id==arrayList.get(i).getProductId()) {
                                test=false;
                                Comment comment=new Comment();
                                System.out.println("Veuillez saisir le commentaire sur le produit :");
                                comment.setContent(scanner.nextLine());
                                comment.setDate(LocalDate.now().toString());
                                comment.setUser(u);
                                comment.setProduct(DAOProduct.selectProductById(id));
                                DAOComment.insertComment(comment);
                                i=arrayList.size();
                            }
                        }
                        if (test) {
                            insertComment(u);
                        }
                    }
                    catch (@SuppressWarnings("unused") Throwable throwable) {
                        insertComment(u);
                    }
            }
        }
    }

    private static void updateUser(User u) {
        System.out.println("Veuillez choisir l'information que vous voulez mettre à jour.\nTapez 1 pour l'adresse e-mail.\nTapez 2 pour le nom.\nTapez 3 pour le prénom.\nTapez 4 pour la date de naissance.\nTapez 5 pour la société.\nTapez 6 pour le mot de passe.\nTapez 7 pour le genre.\nTapez 8 pour le numéro de rue.\nTapez 9 pour le nom de la rue.\nTapez 10 pour le code postal.\nTapez 11 pour la ville.\nTapez 0 pour revenir au menu précédent.");
        switch (scanner.nextLine()) {
            case "0":
                break;
            case "1":
                if (u.getCommentList().size()==0&&u.getRequestList().size()==0) {
                    updateEMail(u);
                }
                else {
                    System.out.println("Cette adresse e-mail est associée à au moins un commentaire ou une commande.\nIl est nécessaire de créer un nouveau compte pour utiliser la nouvelle adresse e-mail.\n");
                    updateUser(u);
                }
                break;
            case "2":
                updateLastName(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "3":
                updateFirstName(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "4":
                updateBirthdate(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "5":
                updateCompany(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "6":
                updatePassword(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "7":
                updateGender(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "8":
                updateNumber(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "9":
                updateStreet(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "10":
                updatePostalCode(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            case "11":
                updateCity(u);
                DAOUser.updateUser(u);
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
                break;
            default:
                if (user.geteMail().equals(u.geteMail())) {
                    updateUser(u);
                }
                else {
                    manageUsers();
                }
        }
    }

    private static void updateEMail(User u) {
        User newU=new User();
        System.out.println("Veuillez saisir le nouvel e-mail :");
        String string=scanner.nextLine();
        if (VerifExpReg.isValidEmail(string)) {
            newU.seteMail(string);
        }
        else {
            System.out.println("Veuillez saisir un format d'email valide.\n");
            updateEMail(u);
        }
        newU.setLastName(u.getLastName());
        newU.setFirstName(u.getFirstName());
        newU.setBirthdate(u.getBirthdate());
        newU.setCompany(u.getCompany());
        newU.setPassword(u.getPassword());
        newU.setGender(u.getGender());
        newU.getAdress().setNumber(u.getAdress().getNumber());
        newU.getAdress().setStreet(u.getAdress().getStreet());
        newU.getAdress().setPostalCode(u.getAdress().getPostalCode());
        newU.getAdress().setCity(u.getAdress().getCity());
        newU.setAuthentificated(u.getAuthentificated());
        newU.setAdmin(u.getAdmin());
        DAOUser.deleteUser(u.geteMail());
        if (user.geteMail().equals(u.geteMail())) {
            user.seteMail(newU.geteMail());
        }
        DAOUser.insertUser(newU);
        if (user.geteMail().equals(newU.geteMail())) {
            updateUser(user);
        }
        else {
            manageUsers();
        }
    }

    private static void manageUsers() {
        System.out.println("Veuillez choisir l'action à effectuer.\nTapez 1 pour afficher la liste des clients.\nTapez 2 pour insérer un autre client.\nTapez 3 pour mettre à jour les informations d'un autre client.\nTapez 4 pour supprimer un autre client.\nTapez 5 pour modérer les commentaires d'un autre client.\nTapez 0 pour revenir au menu précédent.");
        switch (scanner.nextLine()) {
            case "0":
                break;
            case "1":
                displayUsers();
                break;
            case "2":
                insertUserByAdmin();
                break;
            case "3":
                updateUserByAdmin();
                break;
            case "4":
                deleteUserByAdmin();
                break;
            case "5":
                moderateComments();
                break;
            default:
                manageUsers();
        }
    }

    private static void displayUsers() {
        List<User> userList=DAOUser.selectAllUsers();
        for (User c:userList) {
            System.out.println(c.toString());
        }
        manageUsers();
    }

    private static void insertUserByAdmin() {
        System.out.println("Veuillez saisir l'adresse e-mail du client :");
        String string=scanner.nextLine();
        if (VerifExpReg.isValidEmail(string)) {
            User c=DAOUser.selectUserById(string);
            if (c==null) {
                c=new User();
                c.seteMail(string);
                insertUser(c);
                DAOUser.updateUser(c);
            }
            else {
                System.out.println("Ce client est déjà enregistré.\n");
            }
            manageUsers();
        }
        else {
            System.out.println("Veuillez saisir un e-mail valide.\n");
            insertUserByAdmin();
        }
    }

    private static void updateUserByAdmin() {
        System.out.println("Veuillez saisir l'adresse e-mail du client :");
        User c=DAOUser.selectUserById(scanner.nextLine());
        if (c==null) {
            System.out.println("Ce client n'est pas enregistré.");
            manageUsers();
        }
        else {
            updateUser(c);
        }
    }

    private static void deleteUserByAdmin() {
        System.out.println("Veuillez saisir l'adresse e-mail du client :");
        String string=scanner.nextLine();
        User c=DAOUser.selectUserById(string);
        if (c==null) {
            System.out.println("Ce client n'est pas enregistré.\n");
        }
        else {
            DAOUser.deleteUser(string);
        }
        manageUsers();
    }

    private static void moderateComments() {
        System.out.println("Veuillez saisir l'adresse e-mail du client :");
        String string=scanner.nextLine();
        User u=DAOUser.selectUserById(string);
        if (u==null) {
            System.out.println("Ce client n'est pas enregistré.\n");
        }
        else {
            comments(u);
        }
    }

    private static void manageSupply() {
        System.out.println("Veuillez choisir l'action à effectuer.\nTapez 1 pour afficher la liste des produits.\nTapez 2 pour insérer un produit.\nTapez 3 pour mettre à jour un produit.\nTapez 4 pour supprimer un produit.\nTapez 0 pour revenir au menu précédent.");
        switch (scanner.nextLine()) {
            case "0":
                break;
            case "1":
                displayProducts();
                break;
            case "2":
                insertProduct();
                break;
            case "3":
                updateProduct();
                break;
            case "4":
                deleteProduct();
                break;
            default:
                manageSupply();
        }
    }

    private static void displayProducts() {
        List<Product> productList=DAOProduct.selectAllProducts();
        for (Product p:productList) {
            System.out.println(p.toString());
        }
        System.out.println("\n");
        manageSupply();
    }

    private static void insertProduct() {
        try {
            System.out.println("Veuillez saisir l'identifiant du produit :");
            int id=Integer.parseInt(scanner.nextLine());
            Product p=DAOProduct.selectProductById(id);
            if (p==null) {
                p=new Product();
                p.setId(id);
                insertProduct(p);
                DAOProduct.updateProduct(p);
            }
            else {
                System.out.println("Ce produit est déjà enregistré.\n");
            }
            manageSupply();
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            manageSupply();
        }
    }

    private static void insertProduct(Product p) {
        DAOProduct.insertProduct(p);
        updateType(p);
        updateName(p);
        updateAmount(p);
        updatePrice(p);
        updateBreakfast(p);
        updateLunch(p);
        updateSnack(p);
        updateDiner(p);
        updatePopularity(p);
        updateSupply(p);
    }

    private static void updateType(Product p) {
        System.out.println("Veuillez saisir le type du produit :");
        p.setType(scanner.nextLine());
    }

    private static void updateName(Product p) {
        System.out.println("Veuillez saisir le nom du produit :");
        p.setName(scanner.nextLine());
    }

    private static void updateAmount(Product p) {
        try {
            System.out.println("Veuillez la quantité unitaire du produit :");
            int amount=Integer.parseInt(scanner.nextLine());
            if (amount>=0) {
                p.setAmount(amount);
            }
            else {
                updateAmount(p);
            }
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            updateAmount(p);
        }
    }

    private static void updatePrice(Product p) {
        try {
            System.out.println("Veuillez saisir le prix du produit :");
            p.setPrice(Double.parseDouble(new DecimalFormat("#.00").format(scanner.nextLine())));
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            updatePrice(p);
        }
    }

    private static void updateBreakfast(Product p) {
        System.out.println("Veuillez saisir la disponibilité du produit au petit-déjeuner.\nTapez v pour vrai.\nTapez f pour faux.");
        switch (scanner.nextLine().trim().toLowerCase()) {
            case "v":
                p.setBreakfast(true);
                break;
            case "f":
                p.setBreakfast(false);
                break;
            default:
                updateBreakfast(p);
        }
    }

    private static void updateLunch(Product p) {
        System.out.println("Veuillez saisir la disponibilité du produit au déjeuner.\nTapez v pour vrai.\nTapez f pour faux.");
        switch (scanner.nextLine().trim().toLowerCase()) {
            case "v":
                p.setLunch(true);
                break;
            case "f":
                p.setLunch(false);
                break;
            default:
                updateLunch(p);
        }
    }

    private static void updateSnack(Product p) {
        System.out.println("Veuillez saisir la disponibilité du produit au goûter.\nTapez v pour vrai.\nTapez f pour faux.");
        switch (scanner.nextLine().trim().toLowerCase()) {
            case "v":
                p.setSnack(true);
                break;
            case "f":
                p.setSnack(false);
                break;
            default:
                updateSnack(p);
        }
    }

    private static void updateDiner(Product p) {
        System.out.println("Veuillez saisir la disponibilité du produit au dîner.\nTapez v pour vrai.\nTapez f pour faux.");
        switch (scanner.nextLine().trim().toLowerCase()) {
            case "v":
                p.setDiner(true);
                break;
            case "f":
                p.setDiner(false);
                break;
            default:
                updateDiner(p);
        }
    }

    private static void updatePopularity(Product p) {
        try {
            System.out.println("Veuillez saisir la popularité du produit :");
            int popularity=Integer.parseInt(scanner.nextLine());
            if (popularity>=0) {
                p.setPopularity(popularity);
            }
            else {
                updateSupply(p);
            }
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            updateSupply(p);
        }
    }

    private static void updateSupply(Product p) {
        try {
            System.out.println("Veuillez saisir le stock du produit :");
            int supply=Integer.parseInt(scanner.nextLine());
            if (supply>=0) {
                p.setSupply(supply);
            }
            else {
                updateSupply(p);
            }
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            updateSupply(p);
        }
    }

    private static void updateProduct() {
        try {
            System.out.println("Veuillez saisir l'identifiant du produit :");
            Product p=DAOProduct.selectProductById(Integer.parseInt(scanner.nextLine()));
            if (p==null) {
                System.out.println("Ce produit n'est pas enregistré.\n");
            }
            else {
                updateProduct(p);
            }
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            manageSupply();
        }
    }

    private static void updateProduct(Product p) {
        System.out.println("Veuillez choisir l'information que vous voulez mettre à jour.\nTapez 1 pour le type.\nTapez 2 pour le nom.\nTapez 3 pour la quantité unitaire.\nTapez 4 pour le prix.\nTapez 5 pour la disponibilité au petit-déjeuner.\nTapez 6 pour la disponibilité au déjeuner.\nTapez 7 pour la disponibilité au goûter.\nTapez 8 pour la disponibilité au dîner.\nTapez 9 pour la popularité.\nTapez 10 pour le stock.\nTapez 0 pour revenir au menu précédent.");
        switch (scanner.nextLine()) {
            case "0":
                break;
            case "1":
                updateType(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "2":
                updateName(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "3":
                updateAmount(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "4":
                updatePrice(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "5":
                updateBreakfast(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "6":
                updateLunch(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "7":
                updateSnack(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "8":
                updateDiner(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "9":
                updatePopularity(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            case "10":
                updateSupply(p);
                DAOProduct.updateProduct(p);
                updateProduct(p);
                break;
            default:
                updateProduct(p);
        }
    }

    private static void deleteProduct() {
        try {
            System.out.println("Veuillez saisir l'identifiant du produit :");
            int id=Integer.parseInt(scanner.nextLine());
            Product p=DAOProduct.selectProductById(id);
            if (p==null) {
                System.out.println("Ce produit n'est pas enregistré.\n");
            }
            else {
                DAOProduct.deleteProduct(id);
            }
            manageSupply();
        }
        catch (@SuppressWarnings("unused") Throwable throwable) {
            manageSupply();
        }
    }
}