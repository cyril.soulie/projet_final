package dataAccessObject;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DAOComment {
    public static void insertComment(Comment c) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        entityManager.merge(c.getUser());
        entityManager.merge(c.getProduct());
        entityManager.persist(c);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public static ArrayList<Comment> selectCommentByUser(User u) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        ArrayList<Comment> commentList=(ArrayList<Comment>) entityManager.createQuery("SELECT comment FROM Comment comment WHERE user_eMail = ?1",Comment.class).setParameter(1,u.geteMail()).getResultList();
        entityManager.close();
        entityManagerFactory.close();
        return commentList;
    }

    public static ArrayList<Comment> selectCommentByProduct(Product p) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        ArrayList<Comment> commentList=(ArrayList<Comment>) entityManager.createQuery("SELECT comment FROM Comment comment WHERE product_Id = ?1",Comment.class).setParameter(1,Integer.valueOf(p.getId())).getResultList();
        entityManager.close();
        entityManagerFactory.close();
        return commentList;
    }

    public static void updateComment(Comment c) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        Comment comment=entityManager.find(Comment.class,Integer.valueOf(c.getId()));
        comment.setDate(c.getDate());
        comment.setContent(c.getContent());
        comment.setUser(c.getUser());
        comment.setProduct(c.getProduct());
        entityManager.merge(c);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public static void deleteComment(int id) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        Comment comment=entityManager.find(Comment.class,Integer.valueOf(id));
        entityManager.remove(comment);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }
}