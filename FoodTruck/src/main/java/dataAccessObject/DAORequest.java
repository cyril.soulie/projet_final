package dataAccessObject;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DAORequest {
    public static void insertRequest(User u,Request r) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        entityManager.merge(u);
        entityManager.persist(r);
        for (Selection selection:r.getSelectionList()) {
            entityManager.persist(selection);
        }
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public static ArrayList<Request> selectRequestByUser(User u) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        ArrayList<Request> requestList=(ArrayList<Request>) entityManager.createQuery("SELECT request FROM Request request WHERE user_eMail = ?1",Request.class).setParameter(1,u.geteMail()).getResultList();
        entityManager.close();
        entityManagerFactory.close();
        return requestList;
    }
}