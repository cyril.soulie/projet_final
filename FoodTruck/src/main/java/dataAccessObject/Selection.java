package dataAccessObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Selection {
    private int id;
    private int productId;
    private String type;
    private String name;
    private int amount;
    private double price;
    private int quantity;
    private Request request;

    public Selection() {
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id=id;
    }

    public int getProductId() {
        return this.productId;
    }

    public void setProductId(int productId) {
        this.productId=productId;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type=type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount=amount;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price=price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity=quantity;
    }

    @ManyToOne
    @JoinColumn
    public Request getRequest() {
        return this.request;
    }

    public void setRequest(Request request) {
        this.request=request;
    }

    @Override
    public String toString() {
        return "Sélection [ Identifiant du produit : "+this.productId+" , Type de produit : "+this.type+" , Nom du produit : "+this.name+" , Quantité unitaire (g ou ml) "+this.amount+" , Prix unitaire : "+this.price+" , Quantité : "+this.quantity+" ]";
    }
}