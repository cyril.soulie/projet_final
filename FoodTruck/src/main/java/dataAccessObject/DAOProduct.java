package dataAccessObject;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DAOProduct {
    public static void insertProduct(Product p) {
        System.out.println(p.getId());
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        entityManager.persist(p);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public static ArrayList<Product> selectAllProducts() {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        ArrayList<Product> productList=(ArrayList<Product>) entityManager.createQuery("SELECT product FROM Product product",Product.class).getResultList();
        entityManager.close();
        entityManagerFactory.close();
        return productList;
    }

    public static ArrayList<Product> selectProductByRequestType(String string) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        ArrayList<Product> productList=(ArrayList<Product>) entityManager.createQuery("SELECT product FROM Product product WHERE product."+string+" = TRUE AND product.supply > 0",Product.class).getResultList();
        entityManager.close();
        entityManagerFactory.close();
        return productList;
    }

    public static Product selectProductById(int id) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        Product product=entityManager.find(Product.class,Integer.valueOf(id));
        entityManager.close();
        entityManagerFactory.close();
        return product;
    }

    public static void updateProduct(Product p) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        Product product=entityManager.find(Product.class,Integer.valueOf(p.getId()));
        product.setType(p.getType());
        product.setName(p.getName());
        product.setAmount(p.getAmount());
        product.setPrice(p.getPrice());
        product.setBreakfast(p.getBreakfast());
        product.setLunch(p.getLunch());
        product.setSnack(p.getSnack());
        product.setDiner(p.getDiner());
        product.setPopularity(p.getPopularity());
        product.setSupply(p.getSupply());
        entityManager.merge(p);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public static void deleteProduct(int id) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        Product product=entityManager.find(Product.class,Integer.valueOf(id));
        entityManager.remove(product);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }
}