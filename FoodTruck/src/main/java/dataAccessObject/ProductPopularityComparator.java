package dataAccessObject;

import java.util.Comparator;

public class ProductPopularityComparator implements Comparator<Product> {
    @Override
    public int compare(Product p1,Product p2) {
        if (p1.getPopularity()>p2.getPopularity()) {
            return -1;
        }
        else if (p1.getPopularity()==p2.getPopularity()) {
            return 0;
        }
        else {
            return 1;
        }
    }
}