package dataAccessObject;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DAOSelection {
    public static void insertSelection(Selection s) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        entityManager.persist(s);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public static ArrayList<Selection> selectUncommentedSelectionByUser(User u) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        ArrayList<Request> requestList=(ArrayList<Request>) entityManager.createQuery("SELECT request FROM Request request WHERE user_eMail = ?1",Request.class).setParameter(1,u.geteMail()).getResultList();
        ArrayList<Selection> selectionList=new ArrayList<>();
        for (Request r:requestList) {
            for (Selection s:r.getSelectionList()) {
                boolean out=true;
                for (int i=0;i<selectionList.size();i++) {
                    if (s.getProductId()==i) {
                        out=false;
                        i=selectionList.size();
                    }
                }
                if (out) {
                    selectionList.add(s);
                }
            }
        }
        ArrayList<Comment> commentList=(ArrayList<Comment>) entityManager.createQuery("SELECT comment FROM Comment comment WHERE user_eMail = ?1",Comment.class).setParameter(1,u.geteMail()).getResultList();
        ArrayList<Selection> uncommentedSelectionList=new ArrayList<>();
        for (int i=0;i<selectionList.size();i++) {
            boolean uncommented=true;
            for (int j=0;j<commentList.size();j++) {
                if (commentList.get(j).getProduct().getId()==selectionList.get(i).getProductId()) {
                    uncommented=false;
                    j=commentList.size();
                }
            }
            if (uncommented) {
                uncommentedSelectionList.add(selectionList.get(i));
            }
        }
        return uncommentedSelectionList;
    }
}