package dataAccessObject;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DAOUser {
    public static void insertUser(User u) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        entityManager.persist(u);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public static ArrayList<User> selectAllUsers() {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        ArrayList<User> userList=(ArrayList<User>) entityManager.createQuery("SELECT user FROM User user",User.class).getResultList();
        entityManager.close();
        entityManagerFactory.close();
        return userList;
    }

    public static ArrayList<User> selectUserByAdmin() {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        ArrayList<User> userList=(ArrayList<User>) entityManager.createQuery("SELECT user FROM User user WHERE admin = TRUE",User.class).getResultList();
        entityManager.close();
        entityManagerFactory.close();
        return userList;
    }

    public static User selectUserById(String eMail) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        User user=entityManager.find(User.class,eMail);
        entityManager.close();
        entityManagerFactory.close();
        return user;
    }

    public static void updateUser(User u) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        User user=entityManager.find(User.class,u.geteMail());
        user.setLastName(u.getLastName());
        user.setFirstName(u.getFirstName());
        user.setFirstName(u.getFirstName());
        user.setBirthdate(u.getBirthdate());
        user.setCompany(u.getCompany());
        user.setPassword(u.getPassword());
        user.setGender(u.getGender());
        user.setAdress(new Adress());
        user.getAdress().setNumber(u.getAdress().getNumber());
        user.getAdress().setStreet(u.getAdress().getStreet());
        user.getAdress().setPostalCode(u.getAdress().getPostalCode());
        user.getAdress().setCity(u.getAdress().getCity());
        entityManager.merge(u);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    public static void deleteUser(String eMail) {
        EntityManagerFactory entityManagerFactory=Persistence.createEntityManagerFactory("FoodTruck");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        User user=entityManager.find(User.class,eMail);
        entityManager.remove(user);
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityTransaction.commit();
        entityManager.close();
        entityManagerFactory.close();
    }
}