package dataAccessObject;

import javax.persistence.Embeddable;

@Embeddable
public class Adress {
    private String number;
    private String street;
    private String postalCode;
    private String city;

    public Adress() {
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number=number;
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street=street;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode=postalCode;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city=city;
    }

    @Override
    public String toString() {
        return "Adresse [ Numéro : "+this.number+" , Rue : "+this.street+" , Code postal : "+this.postalCode+" , Ville : "+this.city+" ]";
    }
}