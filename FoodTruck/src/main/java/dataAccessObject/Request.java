package dataAccessObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Request {
    private int id;
    private String date;
    private String type;
    private boolean delivery;
    private double price;
    private User user;
    private Collection<Selection> selectionList;

    public Request() {
        this.setPrice(0);
        this.setSelectionList(new ArrayList<Selection>());
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id=id;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date=date;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type=type;
    }

    public boolean getDelivery() {
        return this.delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery=delivery;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price=price;
    }

    @ManyToOne
    @JoinColumn
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user=user;
    }

    @OneToMany(mappedBy="request",fetch=FetchType.EAGER)
    public Collection<Selection> getSelectionList() {
        return this.selectionList;
    }

    public void setSelectionList(Collection<Selection> selectionList) {
        this.selectionList=selectionList;
    }

    @Override
    public String toString() {
        return "Commande [ Identifiant "+this.id+" , Date : "+this.date+" , Type : "+this.type+" , Livraison : "+this.delivery+" , Prix : "+new DecimalFormat("#.00").format(this.price)+" ,\nListe de sélections : "+this.selectionList+" ]";
    }
}