package dataAccessObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Comment {
    private int id;
    private String content;
    private String date;
    private User user;
    private Product product;

    public Comment() {
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id=id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content=content;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date=date;
    }

    @ManyToOne
    @JoinColumn
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user=user;
    }

    @ManyToOne
    @JoinColumn
    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        this.product=product;
    }

    @Override
    public String toString() {
        return "Commentaire [ Identifiant : "+this.id+" , Contenu : "+this.content+" , Date : "+this.date+" , Identifiant utilisateur : "+this.user.geteMail()+" , Identifiant produit : "+this.product.getId()+" ]";
    }
}