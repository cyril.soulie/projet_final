package dataAccessObject;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {
    private String eMail;
    private String lastName;
    private String firstName;
    private String birthdate;
    private String company;
    private String password;
    private String gender;
    private Adress adress;
    private Collection<Comment> commentList;
    private Collection<Request> requestList;
    private boolean authentificated=false;
    private boolean admin=false;

    public User() {
        this.setAdress(new Adress());
        this.setRequestList(new ArrayList<Request>());
        this.setCommentList(new ArrayList<Comment>());
    }

    @Id
    public String geteMail() {
        return this.eMail;
    }

    public void seteMail(String eMail) {
        this.eMail=eMail;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName=lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName=firstName;
    }

    public String getBirthdate() {
        return this.birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate=birthdate;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company=company;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password=password;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender=gender;
    }

    @Embedded
    public Adress getAdress() {
        return this.adress;
    }

    public void setAdress(Adress adress) {
        this.adress=adress;
    }

    @OneToMany(mappedBy="user",fetch=FetchType.EAGER)
    public Collection<Comment> getCommentList() {
        return this.commentList;
    }

    public void setCommentList(Collection<Comment> commentList) {
        this.commentList=commentList;
    }

    @OneToMany(mappedBy="user",fetch=FetchType.EAGER)
    public Collection<Request> getRequestList() {
        return this.requestList;
    }

    public void setRequestList(Collection<Request> requestList) {
        this.requestList=requestList;
    }

    public boolean getAuthentificated() {
        return this.authentificated;
    }

    public void setAuthentificated(boolean authentificated) {
        this.authentificated=authentificated;
    }

    public boolean getAdmin() {
        return this.admin;
    }

    public void setAdmin(boolean admin) {
        this.admin=admin;
    }

    @Override
    public String toString() {
        return "Utilisateur [ E-mail : "+this.eMail+" , Nom : "+this.lastName+" , Prénom : "+this.firstName+" , Date de naissance : "+this.birthdate+" , Société : "+this.company+" , Mot de passe : "+this.password+" , Genre : "+this.gender+" , Adresse : "+this.adress+" ]";
    }
}