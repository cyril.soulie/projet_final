package dataAccessObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Product {
    private int id;
    private String type;
    private String name;
    private int amount;
    private double price;
    private boolean breakfast;
    private boolean lunch;
    private boolean snack;
    private boolean diner;
    private int popularity;
    private int supply;
    private List<Comment> commentList;

    public Product() {
        this.setCommentList(new ArrayList<Comment>());
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id=id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type=type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount=amount;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price=price;
    }

    public boolean getBreakfast() {
        return this.breakfast;
    }

    public void setBreakfast(boolean breakfast) {
        this.breakfast=breakfast;
    }

    public boolean getLunch() {
        return this.lunch;
    }

    public void setLunch(boolean lunch) {
        this.lunch=lunch;
    }

    public boolean getSnack() {
        return this.snack;
    }

    public void setSnack(boolean snack) {
        this.snack=snack;
    }

    public boolean getDiner() {
        return this.diner;
    }

    public void setDiner(boolean diner) {
        this.diner=diner;
    }

    public int getPopularity() {
        return this.popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity=popularity;
    }

    public int getSupply() {
        return this.supply;
    }

    public void setSupply(int supply) {
        this.supply=supply;
    }

    @OneToMany(mappedBy="product",fetch=FetchType.EAGER)
    public List<Comment> getCommentList() {
        return this.commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList=commentList;
    }

    @Override
    public String toString() {
        return "Produit [ Identifiant : "+this.id+" , Type : "+this.type+" , Nom : "+this.name+" , Quantité unitaire (g ou ml) : "+this.amount+" , Prix unitaire : "+new DecimalFormat("#.00").format(this.price)+" , Petit-déjeuner : "+this.breakfast+" , Déjeuner : "+this.lunch+" , Goûter : "+this.snack+" , Dîner : "+this.diner+" , Popularité : "+this.popularity+" , Stock : "+this.supply+" ]";
    }
}