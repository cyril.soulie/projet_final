package test;

import static org.junit.Assert.assertEquals;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import dataAccessObject.DAOProduct;
import dataAccessObject.Product;

public class ProvidersListTest {
    private static final int column=14;
    private static final double profit=1.3;

    //    @SuppressWarnings("static-method")
    //    @Test
    //    public void product_read() {
    //        Logger.getLogger("org.hibernate").setLevel(Level.OFF);
    //        try (BufferedReader bufferedReader=new BufferedReader(new FileReader("product.csv"))) {
    //            String strCurrentLine=bufferedReader.readLine();
    //            while ((strCurrentLine=bufferedReader.readLine())!=null) {
    //                String[] splitLine=strCurrentLine.split(";");
    //                Product p=new Product();
    //                p.setAmount(Integer.parseInt(splitLine[0]));
    //                p.setBreakfast(Boolean.parseBoolean(splitLine[1]));
    //                p.setDiner(Boolean.parseBoolean(splitLine[2]));
    //                p.setLunch(Boolean.parseBoolean(splitLine[3]));
    //                p.setName(splitLine[4]);
    //                p.setPopularity(Integer.parseInt(splitLine[5]));
    //                p.setPrice(Double.parseDouble(new DecimalFormat("#.00",new DecimalFormatSymbols(Locale.ENGLISH)).format(Double.parseDouble(splitLine[6]))));
    //                p.setSnack(Boolean.parseBoolean(splitLine[7]));
    //                p.setSupply(Integer.parseInt(splitLine[8]));
    //                p.setType(splitLine[9]);
    //                DAOProduct.insertProduct(p);
    //            }
    //        }
    //        catch (Throwable throwable) {
    //            System.out.println(throwable.getMessage());
    //        }
    //    }
    @SuppressWarnings("static-method")
    @Test
    public void provider_csv_integrity_test() {
        System.out.println("test");
        Logger.getLogger("org.hibernate").setLevel(Level.OFF);
        ArrayList<String> arrayList=new ArrayList<>();
        try (BufferedReader bufferedReader=new BufferedReader(new FileReader("fournisseurs.txt"))) {
            String string;
            while ((string=bufferedReader.readLine())!=null) {
                arrayList.add(string);
            }
            System.out.println("Fichiers csv : "+arrayList);
        }
        catch (Throwable throwable) {
            System.out.println(throwable.getMessage());
        }
        for (String s:arrayList) {
            try (BufferedReader bufferedReader=new BufferedReader(new FileReader(s+".csv"))) {
                String[] splitted=bufferedReader.readLine().split(";");
                assertEquals(splitted.length,column);
                String strCurrentLine;
                while ((strCurrentLine=bufferedReader.readLine())!=null) {
                    String[] splitLine=strCurrentLine.split(";");
                    Product p=new Product();
                    try {
                        p=DAOProduct.selectProductById(Integer.parseInt(splitLine[0]));
                        p.setPrice(Double.parseDouble(new DecimalFormat("#.00",new DecimalFormatSymbols(Locale.ENGLISH)).format(Double.parseDouble(splitLine[4])*profit)));
                        p.setSupply(p.getSupply()+Integer.parseInt(splitLine[5]));
                        DAOProduct.updateProduct(p);
                    }
                    catch (NullPointerException e) {
                        System.out.println(e.getMessage());
                        p=new Product();
                        System.out.println(p);
                        p.setPrice(Double.parseDouble(new DecimalFormat("#.00",new DecimalFormatSymbols(Locale.ENGLISH)).format(Double.parseDouble(splitLine[4])*profit)));
                        p.setSupply(p.getSupply()+Integer.parseInt(splitLine[5]));
                        DAOProduct.insertProduct(p);
                    }
                }
                System.out.println("Fichier "+s+".csv : OK.");
            }
            catch (@SuppressWarnings("unused") Throwable throwable) {
                System.out.println("Format non valable pour le fichier "+s+".csv.");
            }
        }
        //        userInterface.UserInterface.main(null);
    }
}